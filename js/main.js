$(document).ready(function() {
	$(".product-name").dotdotdot();

	var w = 0;
	$('#pagination ul li').each(function() {
		w = w + ($(this).width() + 30);
	});
	$('#pagination').css('width', w);

	$('.unactive').click(function() {
		return false;
	});

	$('.nav').click(function() {
		if(!$(this).hasClass('nav-active')) {
			$('.nav-active').children('ul').removeAttr('style');
			$('.nav-active').removeClass('nav-active').removeAttr('style').children('p,img').show();
			var tp = parseInt($(this).css('top')) - 50;
			var lp = parseInt($(this).css('left')) - 50;
			var navn = $(this).data('target-menu');
			$(this).addClass('nav-active').css({
				'top' : tp+'px',
				'left' : lp+'px',
			}).children('p,img').hide();
			var mh = $(this).children('ul').height();
			$(this).children('ul').css({
				'margin-top' : -(mh/2)+'px',
				'top': '50%', 
				}).show();
		} else {
			$(this).children('ul').removeAttr('style');
			$(this).removeClass('nav-active').removeAttr('style').children('p,img').show();
		}
	});

	var t = 0;
	$(document).click(function(e) {
		if(t == 1) {
			if (!$(e.target).is("#feedback, #feedback-button, #feedback *")) {
        		$("#feedback").fadeOut();
        		t = 0;
    		}
		}
	});
	$('#feedback-button').click(function() {
		$('#feedback').fadeIn();
		t = 1;
	});
	$('#feedback-close').click(function() {
		$('#feedback').fadeOut();
		t= 0;
	});
});